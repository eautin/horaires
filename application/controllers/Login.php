<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author emman
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
    
    //put your code here
     public function __construct() {
        
        parent::__construct();
        
        // load db model :
        $this->load->model('login_model');
        
        // load helper and libs :
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');

    }
    
    
    public function logout( ) {
        
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('user_password');
        $this->session->unset_userdata('is_logged');
        $this->session->session_destroy();
        redirect('login','index');
        
    }
    
    public function index() {
        
        //by default login controller load login page
        
        $this->load->view('templates/header'); 
        $this->load->view('login/index');
        $this->load->view('templates/footer'); 
    }
    
    
    public function validate_login() {
        
        // form validation and load the admin page 
        
        $this->form_validation->set_rules('userLogin', 'nom utilisateur','trim|required');
        $this->form_validation->set_rules('userPwd', 'password','trim|required');
        
        if ($this->form_validation->run() === FALSE)
        {
          // login error 
            $this->load->view('templates/header');
            $this->load->view('login/index');
            $this->load->view('templates/footer'); 
        
        }
        else
        {
            
            
            $user_data = [
                "user_name" => $this->input->post('userLogin'),
                "user_password" => $this->input->post('userPwd')
                ];
            
            $valid_user = $this->login_model->login_validator($user_data);
           
            // login success
            if($valid_user) {
                
                  // démarrage de la session 
                  $this->session->set_userdata($user_data);
                  $this->session->set_userdata('is_logged', TRUE);
                  //$this->session->set_userdata('role', $valid_user->role);
                  $this->load->model('admin_model');
                  
                  // check role and reroute to specific dashboard
                  
                  $role = $valid_user->role;
                  
                  $data_template = array(
                      
                      "user" => $valid_user,
                      "test" => "coucou"
                  );
                  
                 $this->load->view('templates/header');
                 $this->load->view('/admin/'.$role.'/index', $data_template);
                 $this->load->view('templates/footer');
                 
            }else{
                echo "invalid user";
            }
        }
    }
}
