<div id="bg-login">
   <div class="container">
      <div class="row">
         <div class="col-lg-6 offset-lg-3 login-box spacer-top-bot">
                <?php echo form_open('login/validate_login'); ?>
                    <div class="form-group">
                        <label for="userLogin">Nom d'utilisateur :</label>
                        <input type="text" name="userLogin" class="form-control form-control-lg"/>
                    </div>
                    <div class="form-group">
                        <label for="userPwd">Mot de passe :</label>
                        <input type="password" name="userPwd" class="form-control form-control-lg"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Connexion" class="btn btn-success"/>
                    </div>
               <div class="form-group">
                   
                       <?php echo validation_errors(); ?>
                   
               </div>
         </div>
      </div>
   </div>
</div>