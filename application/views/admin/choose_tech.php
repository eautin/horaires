<div class="container">
    <div class="row">
        <div class="col-lg-6 offset-lg-3 col-md-12 spacer-top-bot">
            <h2>Séléctionner le collaborateur</h2>
            <?php echo form_open('admin/display_tech_calendar'); ?>
            <select name="tech_select" class="form-control">
                <?php  
                foreach($techniciens as $key => $value): 
                    echo'<option value="'.$value["id"].'">'.$value["nom"]." ".$value["prÃ©nom"].'</option>'; 
                endforeach;                
                ?>
            </select>
            <div class="form-group mini-spacer-top-bot">
                <input type="submit" name="submit" value="Valider" class="btn btn-success"/>
            </div>
            <div class="form-group">
                 <?php echo validation_errors(); ?>
            </div>
        </div>
    </div>
</div>