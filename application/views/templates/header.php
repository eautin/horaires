<!doctype html>
<html class="no-js" lang="fr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Accès extranet</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="<?php echo CSS_DIR; ?>style.css">
  <link rel="stylesheet" href="<?php echo CSS_DIR; ?>bootstrap.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
    <header>
        <div class="container ">
          <div class="row">
                <div class="col-lg-12 center">
                    <img src="<?php echo IMG_DIR; ?>tsip.jpg" />
                    <h1 class="mini-spacer-top-bot">Accès Extranet TSIP</h1>
                </div>
                <?php if(isset($_SESSION['is_logged'])){ ?>
              <p><a href="">Se déconnecter</a></p>
                <?php } ?>
           </div>
        </div>
    </header>
    <div class="body-content">