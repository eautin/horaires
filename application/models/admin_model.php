<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_model
 *
 * @author emman
 */
class admin_model extends CI_Model {
    
    public function __construct( ) {
        
       $this->load->database( );
       
    }
    
    public function get_tech_data( ) {
        
        $query = $this->db->query('SELECT * FROM users WHERE role = "technicien"');
        
        return $query->result_array( );
        
    }
    
}
